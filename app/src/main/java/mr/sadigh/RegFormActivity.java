package mr.sadigh;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import mr.sadigh.personaldata.PersonalData;
import mr.sadigh.personaldata.SharedPreferencesPersonalData;

public class RegFormActivity extends AppCompatActivity implements View.OnClickListener {

    PersonalData personalData = new SharedPreferencesPersonalData(this);

    TextView nameTextView;
    TextView familyTextView;
    TextView emailTextView;
    TextView mobileTextView;
    TextView passwordTextView;
    RadioButton genderMale;
    RadioButton genderFemale;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_form);

        bindView();

        if (!personalData.load()) {
            Toast.makeText(this, getResources().getString(R.string.load_error), Toast.LENGTH_SHORT).show();
        } else {
            nameTextView.setText(personalData.getName());
            familyTextView.setText(personalData.getFamily());
            emailTextView.setText(personalData.getEmail());
            mobileTextView.setText(personalData.getMobile());
            passwordTextView.setText(personalData.getPassword());
            genderMale.setChecked(personalData.isMale());
            genderFemale.setChecked(!personalData.isMale());
        }

    }

    private void bindView() {
        nameTextView = findViewById(R.id.name);
        familyTextView = findViewById(R.id.family);
        emailTextView = findViewById(R.id.email);
        mobileTextView = findViewById(R.id.mobile);
        passwordTextView = findViewById(R.id.password);
        genderMale = findViewById(R.id.genderMale);
        genderFemale = findViewById(R.id.genderFemale);
        saveButton = findViewById(R.id.saveRegForm);
        saveButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.saveRegForm) {
            personalData.setName(nameTextView.getText().toString());
            personalData.setFamily(familyTextView.getText().toString());
            personalData.setMale(genderMale.isChecked());
            personalData.setMobile(mobileTextView.getText().toString());
            personalData.setEmail(emailTextView.getText().toString());
            personalData.setPassword(passwordTextView.getText().toString());

            if (personalData.save())
                finish();
            else
                Toast.makeText(this, getResources().getString(R.string.save_error), Toast.LENGTH_SHORT).show();
        }
    }

}
