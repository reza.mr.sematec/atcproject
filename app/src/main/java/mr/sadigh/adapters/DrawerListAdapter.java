package mr.sadigh.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import mr.sadigh.R;

/**
 * Created by Reza on 2017/10/09.
 */

public class DrawerListAdapter extends BaseAdapter {
    ArrayList<String> iconsName;
    ArrayList<String> titles;
    ArrayList<Integer> counters;
    Context mContext;

    public DrawerListAdapter(Context mContext, String[] iconsName, String[] titles, Integer[] counters) {
        super();

        this.iconsName = new ArrayList<String>(Arrays.asList(iconsName));
        this.titles = new ArrayList<String>(Arrays.asList(titles));
        this.counters = new ArrayList<Integer>(Arrays.asList(counters));
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public Object getItem(int i) {
        return titles.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.drawer_item, viewGroup, false);

        TextView title = rowView.findViewById(R.id.drawerItemTitle);
        title.setText(titles.get(i));

        ImageView icon = rowView.findViewById(R.id.drawerItemIcon);
        if (iconsName.get(i) != null) {
            // show icon
        }

        TextView counter = rowView.findViewById(R.id.drawerItemCounter);
        if (counters.get(i) != null) {
            counter.setVisibility(View.VISIBLE);
            counter.setText(String.format("%d", counters.get(i)));
        } else counter.setVisibility(View.INVISIBLE);

        return rowView;


    }

    public void addItem() {
        titles.add(String.format("Item %d", titles.size() + 1));
        iconsName.add(String.format("Item %d", titles.size()));
        if (titles.size() % 2 == 0)
            counters.add(titles.size());
        else counters.add(null);
    }
}
