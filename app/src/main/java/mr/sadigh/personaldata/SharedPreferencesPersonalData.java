package mr.sadigh.personaldata;

import android.content.Context;

import com.orhanobut.hawk.Hawk;

/**
 * Created by Reza on 2017/10/09.
 */

public class SharedPreferencesPersonalData extends PersonalData {

    public SharedPreferencesPersonalData(Context mContext) {
        super(mContext);
    }

    @Override
    public boolean save() {
        Hawk.init(mContext).build();
        Hawk.put("name", getName());
        Hawk.put("family", getFamily());
        Hawk.put("email", getEmail());
        Hawk.put("gender", isMale());
        Hawk.put("mobile", getMobile());
        Hawk.put("password", getPassword());

        return true;
    }


    @Override
    public boolean load() {
        Hawk.init(mContext).build();
//        Toast.makeText(mContext, "load", Toast.LENGTH_LONG).show();
        if (Hawk.contains("name"))
            setName(Hawk.get("name").toString());
        else setName("");
        if (Hawk.contains("family"))
            setFamily(Hawk.get("family").toString());
        else setFamily("");
        if (Hawk.contains("email"))
            setEmail(Hawk.get("email").toString());
        else setEmail("");
        if (Hawk.contains("gender"))
            setMale((Boolean) Hawk.get("gender"));
        else setMale(false);
        if (Hawk.contains("mobile"))
            setMobile(Hawk.get("mobile").toString());
        else setMobile("");
        if (Hawk.contains("password"))
            setPassword(Hawk.get("password").toString());
        else setPassword("");
        return true;
    }
}
