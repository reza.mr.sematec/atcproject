package mr.sadigh.personaldata;

import android.content.Context;

/**
 * Created by Reza on 2017/10/09.
 */

public abstract class PersonalData {
    protected Context mContext;
    private String name;
    private String family;
    private boolean male; // true: male, false: female
    private String mobile;
    private String email;
    private String password;

    public PersonalData(Context mContext) {
        super();
        this.mContext = mContext;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public abstract boolean save();

    public abstract boolean load();
}
