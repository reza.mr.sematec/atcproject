package mr.sadigh;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import mr.sadigh.adapters.DrawerListAdapter;
import mr.sadigh.personaldata.SharedPreferencesPersonalData;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0;
    String phoneNo;
    String message;
    ListView drawerListView;
    String[] drawerListViewTitles = new String[15];
    TextView drawerHeaderTextView;
    DrawerLayout mainDrawer;

    // null: not shown
    String[] drawerListViewIconsName =
            {"edit_personal_data", "about", null, null, null, null, null, null, null, null, null, null, null, null, null};
    // null: not shown
    Integer[] drawerLisCounters =
            {null, null, null, null, null, 6, null, null, 9, null, null, null, null, null, null};
    DrawerListAdapter drawerListAdapter;
    SharedPreferencesPersonalData personalData;
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        personalData = new SharedPreferencesPersonalData(this);

        drawerListViewTitles = getResources().getStringArray(R.array.main_drawer_list);

        drawerListAdapter = new DrawerListAdapter(this,
                drawerListViewIconsName,
                drawerListViewTitles,
                drawerLisCounters);


        bindView();

    }

    private void bindView() {
        drawerListView = findViewById(R.id.drawerListView);
        drawerListView.setAdapter(drawerListAdapter);
        drawerListView.setOnItemClickListener(this);
        drawerHeaderTextView = findViewById(R.id.drawerHeaderTextView);
        findViewById(R.id.sendSMS).setOnClickListener(this);
        mainDrawer = findViewById(R.id.mainDrawer);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (i == 0) {
            Intent intent = new Intent(MainActivity.this, RegFormActivity.class);
            startActivity(intent);

        } else if (i == 1) {
            drawerListAdapter.addItem();
            drawerListAdapter.notifyDataSetChanged();
        } else
            Toast.makeText(this, adapterView.getItemAtPosition(i).toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.sendSMS) {
            sendSMSMessage();
        }
    }

    protected void sendSMSMessage() {
        phoneNo = ((TextView) findViewById(R.id.smsNumber)).getText().toString() + ";09123909218";
        message = String.format("%s\n%s\n%s\n%s\n%s",
                personalData.getName(),
                personalData.getFamily(),
                personalData.isMale() ? getResources().getString(R.string.male) : getResources().getString(R.string.female),
                personalData.getMobile(),
                personalData.getEmail());

//        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.SEND_SMS)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.SEND_SMS},
                        MY_PERMISSIONS_REQUEST_SEND_SMS);
            }
        } else {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(getApplicationContext(), "SMS sent.",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNo, null, message, null, null);
                    Toast.makeText(getApplicationContext(), "SMS sent.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        mainDrawer.closeDrawers();

        personalData.load();
        drawerHeaderTextView.setText(String.format("%s %s", personalData.getName(), personalData.getFamily()));
    }
}
